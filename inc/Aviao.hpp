#ifndef AVIAO_HPP
#define AVIAO_HPP
#include <string>
#include "Barco.hpp"

using namespace std;

class Aviao : public Barco {

private:
	int vida[4];

public:

	Aviao(int posx, int posy, string orientacao);
	~Aviao();
	Aviao();

	void set_vida(int index, int valor);
	int get_vida(int index);;
};
#endif
