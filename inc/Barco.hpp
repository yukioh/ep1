#ifndef BARCO_HPP
#define BARCO_HPP
#include <string>

using namespace std;

class Barco {

private:
	//  string tipo;
	int altura;
	int largura;
	string orientacao;
	int tamanho;
	string tipo;


public:
	Barco(int altura, int largura,  string orientacao);
	~Barco();
	Barco();

	string get_tipo();
	void set_tipo(string tipo);

	void set_altura(int altura);
	int get_altura();

	void set_largura(int largura);
	int get_largura();

	void set_orientacao(string orientacao);
	string get_orientacao();

	void set_tamanho(int tamanho);
	int get_tamanho();

	int Life();
};

#endif
