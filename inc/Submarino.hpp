#ifndef SUBMARINO_HPP
#define SUBMARINO_HPP
#include <string>
#include "Barco.hpp"

using namespace std;

class Submarino : public Barco {

private:

	int vida[2];

public:

	Submarino(int posx, int posy, string orientacao);
	~Submarino();
	Submarino();

	void set_vida(int index, int valor);
	int get_vida(int index);
};
#endif
