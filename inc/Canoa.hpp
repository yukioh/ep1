#ifndef CANOA_HPP
#define CANOA_HPP
#include <string>
#include "Barco.hpp"

using namespace std;

class Canoa : public Barco {

private:

	int  vida[1];

public:

	Canoa(int posx, int posy, string orientacao);
	~Canoa();
	Canoa();

	void set_vida(int index, int valor);
	int get_vida(int index);
};
#endif
