#include "Aviao.hpp"
#include <string>
#include <iostream>

using namespace std;

Aviao::Aviao(int y, int x, string orientacao) {

	set_altura(y);
	set_largura(x);
	set_orientacao(orientacao);
	set_tamanho(2);
	set_tipo("Porta-Aviao");
	for (int i = 0; i < 4; i++) {

		set_vida(i, 1);
	}
}

Aviao::~Aviao() {

	cout << "Aviao destruido!" << endl;
}

void Aviao::set_vida(int index, int valor) {
	this->vida[index] = valor;
}
int Aviao::get_vida(int index) {
	return vida[index];
}
