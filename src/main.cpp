#include <iostream>
#include <string>
#include <fstream>
#include <vector>
#include "Barco.hpp"
#include "Canoa.hpp"
#include "Submarino.hpp"
#include "Aviao.hpp"

using namespace std;

void regras() {

	int sair;

	cout << "Antes do jogo comecar todos os barcos ja seram alocados sendo que:" << endl << "-As canoas sao representados pelo numero 1" << endl << "-Os submarinos pelo numero 2 e 4 qnd está danificado" << endl << "-Os porta-avioes pelos numeros 3" << endl << "-Agua pelo numero 9" << endl << "-Barcos destruido pelo numero 5" << endl;

	cout << "#No caso de alguma embarcacao sobrepor a outra embarcacao, a de maior tamanho prevalecera." << endl << "#ganha o jogador que destruir todas as embarcações do inimigo primeiro." << endl << endl;

	cout << "Digite 1 para sair." << endl << endl;

	cin >> sair;



}


void start() {
	/*
	Procedimento para ler as configurações dos tabuleiros
	*/
	ifstream mapatxt;
	string data;
	string auxParser, orientacao;
	int pos[2], i;
	string tipo_barco;
	bool lendoJogador1;

	//Criação do Mapa
	int mapa_p1[13][13] = {}, mapa_p2[13][13] = {}, ini_p1[13][13] = {}, ini_p2[13][13] = {};



	vector<Barco*> jogador1, jogador2;

	mapatxt.open("../ep1/map_1.txt");
	// Tratamento de Erro
	if (!mapatxt) {
		cout << "Arquivo não encontrado." << endl;
	}
	lendoJogador1 = true;
	// PROCEDIMENTO DE IGNORAR #
	getline(mapatxt, data);
	while (!mapatxt.eof()) {
		// CONDIÇÃO DE COMENTÁRIO
		if (data[0] == '#' || data.length() == 0) {
			if (data == "# player_2") {
				lendoJogador1 = false;
			}
			getline(mapatxt, data);
		}
		// condição de dados
		else {
			i = 0;
			auxParser = "";
			// Lendo Linha
			while (data[i] != ' ') {
				auxParser += data[i];
				i++;
			}
			i++;
			pos[0] = stoi(auxParser);
			auxParser = "";
			// Lendo coluna
			while (data[i] != ' ') {
				auxParser += data[i];
				i++;
			}
			i++;
			pos[1] = stoi(auxParser);
			auxParser = "";
			// Lendo tipo
			while (data[i] != ' ') {
				auxParser += data[i];
				i++;
			}
			i++;
			tipo_barco = auxParser;
			auxParser = "";
			// Lendo Orientação
			while (data[i] != '\0') {
				auxParser += data[i];
				i++;
			}

			orientacao = auxParser;

			getline(mapatxt, data);

			// Instanciação de Embarcacao para os jogadores
			// TODO, FAZER VÁRIOS IF,ELSE PARA CADA TIPO DE EMBARCAÇÃO
			if (lendoJogador1) {
				if (tipo_barco == "canoa")
				{
					jogador1.push_back(new Canoa(pos[0], pos[1], orientacao));
				}
				else if (tipo_barco == "submarino") {
					jogador1.push_back(new Submarino(pos[0], pos[1], orientacao));
				}
				else {
					jogador1.push_back(new Aviao(pos[0], pos[1], orientacao));
				}
			}
			else {
				if (tipo_barco == "canoa")
				{
					jogador2.push_back(new Canoa(pos[0], pos[1], orientacao));
				}
				else if (tipo_barco == "submarino") {
					jogador2.push_back(new Submarino(pos[0], pos[1], orientacao));
				}
				else {
					jogador2.push_back(new Aviao(pos[0], pos[1], orientacao));
				}
			}

		}



	}

	mapatxt.close();

	//Alocar barquinhos em formato inteiro canoa=1, sub=2, porta-aviao=3 e agua=0.
	for (int i = 0; i < 13; i++) {
		for (int j = 0; j < 13; j++) {

			if (i == jogador1[0]->get_largura() && j == jogador1[0]->get_altura())
				mapa_p1[i][j] = 1;
			else if (i == jogador1[1]->get_largura() && j == jogador1[1]->get_altura())
				mapa_p1[i][j] = 1;
			else if (i == jogador1[2]->get_largura() && j == jogador1[2]->get_altura())
				mapa_p1[i][j] = 1;
			else if (i == jogador1[3]->get_largura() && j == jogador1[3]->get_altura())
				mapa_p1[i][j] = 1;
			else if (i == jogador1[4]->get_largura() && j == jogador1[4]->get_altura())
				mapa_p1[i][j] = 1;
			else if (i == jogador1[5]->get_largura() && j == jogador1[5]->get_altura())
				mapa_p1[i][j] = 1;

			if (i == jogador1[6]->get_largura() && j == jogador1[6]->get_altura()) {
				if (jogador1[6]->get_orientacao() == "direita") {
					mapa_p1[i][j] = 2;
					mapa_p1[i][j+1] = 2;
				}
				else if (jogador1[6]->get_orientacao() == "esquerda") {
					mapa_p1[i][j] = 2;
					mapa_p1[i][j-1] = 2;
				}
				else if (jogador1[6]->get_orientacao() == "cima") {
					mapa_p1[i][j] = 2;
					mapa_p1[i-1][j] = 2;
				}
			}
			else if (i == jogador1[7]->get_largura() && j == jogador1[7]->get_altura()) {
				if (jogador1[7]->get_orientacao() == "direita") {
					mapa_p1[i][j] = 2;
					mapa_p1[i][j + 1] = 2;
				}
				else if (jogador1[7]->get_orientacao() == "esquerda") {
					mapa_p1[i][j] = 2;
					mapa_p1[i][j - 1] = 2;
				}
				else if (jogador1[7]->get_orientacao() == "cima") {
					mapa_p1[i][j] = 2;
					mapa_p1[i - 1][j] = 2;
				}
			}
			else if (i == jogador1[8]->get_largura() && j == jogador1[8]->get_altura()) {
				if (jogador1[8]->get_orientacao() == "direita") {
					mapa_p1[i][j] = 2;
					mapa_p1[i][j + 1] = 2;
				}
				else if (jogador1[8]->get_orientacao() == "esquerda") {
					mapa_p1[i][j] = 2;
					mapa_p1[i][j - 1] = 2;
				}
				else if (jogador1[8]->get_orientacao() == "cima") {
					mapa_p1[i][j] = 2;
					mapa_p1[i - 1][j] = 2;
				}
			}
			if (i == jogador1[9]->get_largura() && j == jogador1[9]->get_altura()) {
				if (jogador1[9]->get_orientacao() == "direita") {
					mapa_p1[i][j] = 2;
					mapa_p1[i][j + 1] = 2;
				}
				else if (jogador1[9]->get_orientacao() == "esquerda") {
					mapa_p1[i][j] = 2;
					mapa_p1[i][j - 1] = 2;
				}
				else if (jogador1[9]->get_orientacao() == "cima") {
					mapa_p1[i][j] = 2;
					mapa_p1[i - 1][j] = 2;
				}
			}

			else if (i == jogador1[10]->get_largura() && j == jogador1[10]->get_altura()) {
				if (jogador1[10]->get_orientacao() == "direita") {
					mapa_p1[i][j] = 3;
					mapa_p1[i][j+1] = 3;
					mapa_p1[i][j+2] = 3;
					mapa_p1[i][j+3] = 3;
				}
				else if (jogador1[10]->get_orientacao() == "esquerda") {
					mapa_p1[i][j] = 3;
					mapa_p1[i][j-1] = 3;
					mapa_p1[i][j-2] = 3;
					mapa_p1[i][j-3] = 3;
				}
				else if (jogador1[10]->get_orientacao() == "cima") {
					mapa_p1[i][j] = 3;
					mapa_p1[i-1][j] = 3;
					mapa_p1[i-2][j] = 3;
					mapa_p1[i-3][j] = 3;
				}
			}
			else if (i == jogador1[11]->get_largura() && j == jogador1[11]->get_altura()) {
				if (jogador1[11]->get_orientacao() == "direita") {
					mapa_p1[i][j] = 3;
					mapa_p1[i][j + 1] = 3;
					mapa_p1[i][j + 2] = 3;
					mapa_p1[i][j + 3] = 3;
				}
				else if (jogador1[11]->get_orientacao() == "esquerda") {
					mapa_p1[i][j] = 3;
					mapa_p1[i][j - 1] = 3;
					mapa_p1[i][j - 2] = 3;
					mapa_p1[i][j - 3] = 3;
				}
				else if (jogador1[11]->get_orientacao() == "cima") {
					mapa_p1[i][j] = 3;
					mapa_p1[i - 1][j] = 3;
					mapa_p1[i - 2][j] = 3;
					mapa_p1[i - 3][j] = 3;
				}
			}


		}
	}



	for (int i = 0; i < 13; i++) {
		for (int j = 0; j < 13; j++) {

			if (i == jogador2[0]->get_largura() && j == jogador2[0]->get_altura())
				mapa_p2[i][j] = 1;
			else if (i == jogador2[1]->get_largura() && j == jogador2[1]->get_altura())
				mapa_p2[i][j] = 1;
			else if (i == jogador2[2]->get_largura() && j == jogador2[2]->get_altura())
				mapa_p2[i][j] = 1;
			else if (i == jogador2[3]->get_largura() && j == jogador2[3]->get_altura())
				mapa_p2[i][j] = 1;
			else if (i == jogador2[4]->get_largura() && j == jogador2[4]->get_altura())
				mapa_p2[i][j] = 1;
			else if (i == jogador2[5]->get_largura() && j == jogador2[5]->get_altura())
				mapa_p2[i][j] = 1;

			if (i == jogador2[6]->get_largura() && j == jogador2[6]->get_altura()) {
				if (jogador2[6]->get_orientacao() == "direita") {
					mapa_p2[i][j] = 2;
					mapa_p2[i][j+1] = 2;
				}
				else if (jogador2[6]->get_orientacao() == "esquerda") {
					mapa_p2[i][j] = 2;
					mapa_p2[i][j-1] = 2;
				}
				else if (jogador2[6]->get_orientacao() == "cima") {
					mapa_p2[i][j] = 2;
					mapa_p2[i - 1][j] = 2;
				}
			}
			else if (i == jogador2[7]->get_largura() && j == jogador2[7]->get_altura()) {
				if (jogador2[7]->get_orientacao() == "direita") {
					mapa_p2[i][j] = 2;
					mapa_p2[i][j+1] = 2;
				}
				else if (jogador2[7]->get_orientacao() == "esquerda") {
					mapa_p2[i][j] = 2;
					mapa_p2[i][j-1] = 2;
				}
				else if (jogador2[7]->get_orientacao() == "cima") {
					mapa_p2[i][j] = 2;
					mapa_p2[i - 1][j] = 2;
				}
			}
			else if (i == jogador2[8]->get_largura() && j == jogador2[8]->get_altura()) {
				if (jogador2[8]->get_orientacao() == "direita") {
					mapa_p2[i][j] = 2;
					mapa_p2[i][j+1] = 2;
				}
				else if (jogador2[8]->get_orientacao() == "esquerda") {
					mapa_p2[i][j] = 2;
					mapa_p2[i][j-1] = 2;
				}
				else if (jogador2[8]->get_orientacao() == "cima") {
					mapa_p2[i][j] = 2;
					mapa_p2[i - 1][j] = 2;
				}
			}
			else if (i == jogador2[9]->get_largura() && j == jogador2[9]->get_altura()) {
				if (jogador2[9]->get_orientacao() == "direita") {
					mapa_p2[i][j] = 2;
					mapa_p2[i][j+1] = 2;
				}
				else if (jogador2[9]->get_orientacao() == "esquerda") {
					mapa_p2[i][j] = 2;
					mapa_p2[i][j-1] = 2;
				}
				else if (jogador2[9]->get_orientacao() == "cima") {
					mapa_p2[i][j] = 2;
					mapa_p2[i - 1][j] = 2;
				}
			}

			if (i == jogador2[10]->get_largura() && j == jogador2[10]->get_altura()) {
				if (jogador2[10]->get_orientacao() == "direita") {
					mapa_p2[i][j] = 3;
					mapa_p2[i][j + 1] = 3;
					mapa_p2[i][j + 2] = 3;
					mapa_p2[i][j + 3] = 3;
				}
				else if (jogador2[10]->get_orientacao() == "esquerda") {
					mapa_p2[i][j] = 3;
					mapa_p2[i][j - 1] = 3;
					mapa_p2[i][j - 2] = 3;
					mapa_p2[i][j - 3] = 3;
				}
				else if (jogador2[10]->get_orientacao() == "cima") {
					mapa_p2[i][j] = 3;
					mapa_p2[i - 1][j] = 3;
					mapa_p2[i - 2][j] = 3;
					mapa_p2[i - 3][j] = 3;
				}
			}
			else if (i == jogador2[11]->get_largura() && j == jogador2[11]->get_altura()) {
				if (jogador2[11]->get_orientacao() == "direita") {
					mapa_p2[i][j] = 3;
					mapa_p2[i][j + 1] = 3;
					mapa_p2[i][j + 2] = 3;
					mapa_p2[i][j + 3] = 3;
				}
				else if (jogador2[11]->get_orientacao() == "esquerda") {
					mapa_p2[i][j] = 3;
					mapa_p2[i][j - 1] = 3;
					mapa_p2[i][j - 2] = 3;
					mapa_p2[i][j - 3] = 3;
				}
				else if (jogador2[11]->get_orientacao() == "cima") {
					mapa_p2[i][j] = 3;
					mapa_p2[i - 1][j] = 3;
					mapa_p2[i - 2][j] = 3;
					mapa_p2[i - 3][j] = 3;
				}
			}


		}
	}


	//vida do player ################################################################################################################

	int vida_p1=0, vida_p2=0;
	for (int coluna = 0; coluna < 13; coluna++) {
		for (int linha = 0; linha < 13; linha++) {
			if(mapa_p1[coluna][linha] > 0)
				vida_p1++;
		}
	}
	for (int coluna = 0; coluna < 13; coluna++) {
		for (int linha = 0; linha < 13; linha++) {
			if (mapa_p2[coluna][linha] > 0)
				vida_p2++;
		}
	}


	//Implementacao dos turnos ######################################################################################################
	int parada = 1, para1=1, para2=1, v, win1=0, win2=0;
	int tiro1, tiro2;

	string ok, nome_p1, nome_p2;

	cout << "Digite o nome do jogador 1: ";

	cin >> nome_p1;

	cout << "Digite o nome do jogador 2: ";

	cin >> nome_p2;

	cout << "O mapa de cima mostra suas embarcacoes, o outro logo abaixo mostra onde voce ja atirou." << endl;


		while (parada) {

			//turno do jogador 1 ####################################################################################################

			cout << endl << endl << endl << "Turno do " << nome_p1 << "!"<< endl;

			cout << "digite 'ok' para continuar" << endl << endl;

			cin >> ok;
			//seu mapa
			for(int coluna=0; coluna<13; coluna++){
				if (para1) {
					cout << "   0   1   2   3   4   5   6   7   8   9   10  11  12" << endl;
					para1 = 0;
				}
				cout << coluna;

				if (coluna <= 9)
					cout << " ";

				for (int linha = 0; linha < 13; linha++) {
					cout << "[" << mapa_p1[coluna][linha] << "]" << " ";
				}
				cout << endl;
			}
			//mapa inimigo
			for (int coluna = 0; coluna < 13; coluna++) {
				if (para2) {
					cout << "   1   2   3   4   5   6   7   8   9   10  11  12  13" << endl;
					para2 = 0;
				}
				cout << coluna;

				if (coluna <= 9)
					cout << " ";

				for (int linha = 0; linha < 13; linha++) {
					cout << "[" << ini_p1[coluna][linha] << "]" << " ";
				}
				cout << endl;
			}

			cout << endl << "Digite a coluna que deseja atacar: ";

			cin >> tiro2;

			cout << endl << "Digite a linha que deseja atacar: ";

			cin >> tiro1;

			cout << endl;

			if (mapa_p2[tiro1][tiro2] == 1) {
				mapa_p2[tiro1][tiro2] = 5;
				cout << "Canoa destruida!" << endl;
				ini_p1[tiro1][tiro2] = 9;
				vida_p2--;
			}
			else if (mapa_p2[tiro1][tiro2] == 2) {
				mapa_p2[tiro1][tiro2] = 4;
				cout << "Acertou o Submarino!" << endl;
				ini_p1[tiro1][tiro2] = 4;
			}
			else if (mapa_p2[tiro1][tiro2] == 4) {
				mapa_p2[tiro1][tiro2] = 5;
				cout << "Destruiu parte o Submarino!" << endl;
				ini_p1[tiro1][tiro2] = 5;
				vida_p2--;
			}
			else if (mapa_p2[tiro1][tiro2] == 3) {

				v = rand() % 100 + 1;

				if (v > 30) {
					mapa_p2[tiro1][tiro2] = 5;
					cout << "Acertou o porta-aviao!" << endl;
					ini_p1[tiro1][tiro2] = 5;
					vida_p2--;
				}
				else
					cout << "O porta aviao se defendeu" << endl;
			}
			else if (mapa_p2[tiro1][tiro2] == 0) {
				mapa_p2[tiro1][tiro2] = 9;
				cout << "Agua!" << endl;
				ini_p1[tiro1][tiro2] = 9;
			}
			else if (mapa_p2[tiro1][tiro2] == 9) {
				mapa_p2[tiro1][tiro2] = 9;
				cout << "Agua! DENOVO!!!!!" << endl;
				ini_p1[tiro1][tiro2] = 9;
			}
			else if (mapa_p2[tiro1][tiro2] == 5) {
				mapa_p2[tiro1][tiro2] = 5;
				cout << "EMBARCACAO JA FOI DESTRUIDA!!!!!" << endl;
				ini_p1[tiro1][tiro2] = 5;
			}
			para1 = 1;
			para2 = 1;

			if (vida_p2 == 0) {
				win1 = 1;
				break;
			}

			//Turno do jogador 2 ####################################################################################################

			cout << endl << endl << endl << "Turno de " << nome_p2 << "!" << endl;

			cout << "digite 'ok' para continuar" << endl << endl;

			cin >> ok;
			//seu mapa
			for (int coluna = 0; coluna < 13; coluna++) {
				if (para1) {
					cout << "   0   1   2   3   4   5   6   7   8   9   10  11  12" << endl;
					para1 = 0;
				}
				cout << coluna;

				if (coluna <= 9)
					cout << " ";

				for (int linha = 0; linha < 13; linha++) {
					cout << "[" << mapa_p2[coluna][linha] << "]" << " ";
				}
				cout << endl;
			}
			//mapa inimigo
			for (int coluna = 0; coluna < 13; coluna++) {
				if (para2) {
					cout << "   0   1   2   3   4   5   6   7   8   9   10  11  12" << endl;
					para2 = 0;
				}
				cout << coluna;

				if (coluna <= 9)
					cout << " ";

				for (int linha = 0; linha < 13; linha++) {
					cout << "[" << ini_p2[coluna][linha] << "]" << " ";
				}
				cout << endl;
			}

			cout << endl << "Digite a coluna que deseja atacar: " ;

			cin >> tiro2;

			cout << endl << "Digite a linha que deseja atacar: ";

			cin >> tiro1;

			cout << endl;

			if (mapa_p1[tiro1][tiro2] == 1) {
				mapa_p1[tiro1][tiro2] = 5;
				cout << "Canoa destruida!" << endl;
				ini_p2[tiro1][tiro2] = 5;
				vida_p1--;
			}
			else if (mapa_p1[tiro1][tiro2] == 2) {
				mapa_p1[tiro1][tiro2] = 4;
				cout << "Acertou o Submarino!" << endl;
				ini_p2[tiro1][tiro2] = 4;
			}
			else if (mapa_p1[tiro1][tiro2] == 4) {
				mapa_p1[tiro1][tiro2] = 5;
				cout << "Destruiu parte o Submarino!" << endl;
				ini_p2[tiro1][tiro2] = 5;
				vida_p1--;
			}
			else if (mapa_p1[tiro1][tiro2] == 3) {

				v = rand() % 100 + 1;

				if (v > 30) {
					mapa_p1[tiro1][tiro2] = 5;
					cout << "Acertou o porta-aviao!" << endl;
					ini_p2[tiro1][tiro2] = 5;
					vida_p1--;
				}
				else
					cout << "O porta aviao se defendeu" << endl;
			}
			else if (mapa_p1[tiro1][tiro2] == 0) {
				mapa_p1[tiro1][tiro2] = 9;
				cout << "Agua!" << endl;
				ini_p2[tiro1][tiro2] = 9;
			}
			else if (mapa_p1[tiro1][tiro2] == 9) {
				mapa_p1[tiro1][tiro2] = 9;
				cout << "Agua! DENOVO!!!!!" << endl;
				ini_p2[tiro1][tiro2] = 9;
			}
			else if (mapa_p1[tiro1][tiro2] == 5) {
				mapa_p1[tiro1][tiro2] = 5;
				cout << "EMBARCACAO JA FOI DESTRUIDA!!!!!" << endl;
				ini_p2[tiro1][tiro2] = 5;
			}
			para1 = 1;
			para2 = 1;

			if (vida_p1 == 0) {
				win1 = 1;
				break;
			}
		}

		if (win1) {
			cout << endl << "Parabens jogador 1 venceu!" << endl;
		}
		else {
			cout << endl << "Parabens jogador 2 venceu!" << endl;
		}

	/*for (i = 0; i < jogador1.size(); i++) {
		cout << "BARCO NUMERO " << i+1 << " P1" << endl;
		cout << "TIPO: " << jogador1[i]->get_tipo() << endl;
		cout << "POSICAO: " << jogador1[i]->get_altura() << " " << jogador1[i]->get_largura() << endl;
		cout << endl << endl;
	}

	for (i = 0; i < jogador2.size(); i++) {
		cout << "BARCO NUMERO " << i+1 << "P2" << endl;
		cout << "TIPO: " << jogador2[i]->get_tipo() << endl;
		cout << "POSICAO: " << jogador2[i]->get_altura() << " " << jogador2[i]->get_largura() << endl;
		cout << endl << endl;
	}*/
	//Tabuleiro m1 = new Tabuleiro(1,barco);
	//Tabuleiro m2 = new Tabuleiro(2,barco);


}


void menu() {

	int selecao;

	cout << "#####       ####     ########     ####     ##      ##    ##     ####         ##    ##     ####     ##    ##  " << endl;
	cout << "##   ##    ##  ##       ##       ##  ##    ##      ##    ##    ##  ##        ####  ##    ##  ##    ##    ##  " << endl;
	cout << "#####     ########      ##      ########   ##      ########   ########   ##  ## ## ##   ########    ##  ##   " << endl;
	cout << "##   ##  ##      ##     ##     ##      ##  ##      ##    ##  ##      ##      ##  ####  ##      ##    ####    " << endl;
	cout << "#####    ##      ##     ##     ##      ##  ######  ##    ##  ##      ##      ##   ###  ##      ##     ##     " << endl;

	cout << endl << endl << "MENU" << endl;
	cout << endl << "Aperte 1 para jogar" << endl;
	cout << "Aperte 2 para sair" << endl;
	cout << "Aperte 3 para ler as regras" << endl;

	cout << endl << "Digite aqui: ";

	cin >> selecao;

	switch (selecao) {

	case(1):
		start();
		break;
	case(2):
		break;
	case(3):
		regras();
	default:
		break;
	}
}


int main() {

	menu();

	return 0;
}
