#include "Canoa.hpp"
#include <string>
#include <iostream>

using namespace std;

Canoa::Canoa(int y, int x, string orientacao) {

	set_altura(y);
	set_largura(x);
	set_orientacao(orientacao);
	set_tamanho(1);
	set_tipo("Canoa");
	for (int i = 0; i < 1; i++) {

		set_vida(i, 1);
	}
}

Canoa::~Canoa() {

	cout << "Canoa destruida!" << endl;
}

void Canoa::set_vida(int index, int valor) {
	this->vida[index] = valor;
}
int Canoa::get_vida(int index) {
	return vida[index];
}
