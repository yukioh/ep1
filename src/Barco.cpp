#include <string>
#include "Barco.hpp"

using namespace std;

Barco::Barco(int altura, int largura,  string orientacao)
{
	this->set_altura(altura);
	this->set_largura(largura);
	this->set_orientacao(orientacao);
	tamanho = 1;

}
Barco::~Barco() {}
Barco::Barco() {}

//getter e setters


void Barco::set_altura(int altura) {
	this->altura = altura;
}
int Barco::get_altura() {
	return altura;
}

void Barco::set_largura(int largura) {
	this->largura = largura;
}
int Barco::get_largura() {
	return largura;
}


void Barco::set_orientacao(string orientacao) {
	this->orientacao = orientacao;
}
string Barco::get_orientacao() {
	return orientacao;
}


void Barco::set_tamanho(int tamanho) {
	this->tamanho = tamanho;
}
int Barco::get_tamanho() {
	return tamanho;
}

void Barco::set_tipo(string tipo) {
	this->tipo = tipo;
}
string Barco::get_tipo() {
	return tipo;
}
