#include "Submarino.hpp"
#include <string>
#include <iostream>

using namespace std;

Submarino::Submarino(int y, int x, string orientacao) {

	set_altura(y);
	set_largura(x);
	set_orientacao(orientacao);
	set_tipo("Submarino");
	set_tamanho(2);
	for (int i = 0; i < 2; i++) {

		set_vida(i, 2);
	}
}

Submarino::~Submarino() {

	cout << "Submarino destruido!" << endl;
}

void Submarino::set_vida(int index, int valor) {
	this->vida[index] = valor;
}
int Submarino::get_vida(int index) {
	return vida[index];
}
