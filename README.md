# EP1 - OO 2019.1 (UnB - Gama)

&nbsp;&nbsp;Este projeto consiste em criar um jogo em C++ similar ao game Battleship (ou batalha naval), onde todas suas instruções, dicas e requisitos para a avaliação estão sendo descritos na [wiki](https://gitlab.com/oofga/eps/eps_2019_1/ep1/wikis/home) do repositório.

## Instruções

&nbsp;&nbsp;Recomenda-se construir o corpo do README com no mínimo no seguinte formato:

* Descrição do projeto
* Instruções de execução

&nbsp;&nbsp;De forma básica, a construção e execução do projeto funciona da seguinte maneira:

Para construir os objetos definidos pelo projeto, juntamente com o binário para realizar a execução:
```
$ make
```

Para executar o binário criado e iniciar o programa:
```
$ make run
```

Caso já tenha, posteriormente a uma mudança, criado os objetos definidos, lembre-se sempre de criar o objeto dos novos, limpando os antigos:
```
$ make clean
```

## Observações

* Fique avontade para manter o README da forma que achar melhor, mas é importante manter ao menos as informações descritas acima para, durante a correção, facilitar a avaliação.
* Divirtam-se mas sempre procurando atender aos critérios de avaliação informados na Wiki, ela definirá a nota a ser atribuída ao projeto.

Notas do aluno:

  - Em caso do programa não encontrar o arquivo txt, no caso mapa_1.txt. Pode ser em razão de eu ter feito o trabalho majoritariamente no Visual Studio, mudando o local que o arquivo.txt está localizado.
  - caso o programa copile e execute, ao entrar no menu de regras você terá que executar o programa mais uma vez, mas agora selecionando a opção "1" (JOGAR).
  - Caso o programe não copile como arquivo make e make run. Tentar usar o Visual Studio para rodar o programa e atualize o mapatxt.open("../ep1/mapa_1.txt"); para mapatxt.open("../Batalha_Naval/mapa_1.txt");.
